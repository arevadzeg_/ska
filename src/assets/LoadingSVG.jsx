import { Component } from "react";


export class LoadingSVG extends Component {


    render() {
        return <svg width="200px" height="200px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
            <circle cx="50" cy="50" fill="none" stroke="#5ece7b" strokeWidth="3" r="24" strokeDasharray="113.09733552923255 39.69911184307752" >
                <animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" dur="1.075268817204301s" values="0 50 50;360 50 50" keyTimes="0;1" />
            </circle>
        </svg>
    }

}