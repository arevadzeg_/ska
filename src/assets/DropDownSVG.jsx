import { Component } from "react";


export class DropDownSVG extends Component {

    render() {

        return <svg xmlns="http://www.w3.org/2000/svg" className={this.props.className} width="8" height="4" viewBox="0 0 8 4" fill="none">
            <path d="M1 0.5L4 3.5L7 0.5" stroke="black" strokeLinecap="round" strokeLinejoin="round" />
        </svg>
    }
}