import _ from "lodash"



const deepCheck = (cart, productId, selectedAttributes) => {
    let changeQuantity = false
    const updatedCart = cart.length > 0 && JSON.parse(localStorage.getItem('cart')).map((cartItem) => {
        if (cartItem.id === productId) {
            if(_.isEqual(cartItem.selectedAttributes,selectedAttributes )){
                changeQuantity = true
                return { ...cartItem, count: cartItem.count + 1} 
            }
        }
        return cartItem
    })
    return {updatedCart, changeQuantity}
}

export default deepCheck