import { Component } from "react";
import { connect } from "react-redux";
import ChangeQuantity from "../../components/ChangeQuantity";
import Price from "../../components/Price";
import { ProductAttribute } from "../../components/ProductAttribute";
import Total from "../../components/Total";
import { setCart, setCartTotal } from "../../redux/actions";
import './Cart.css'
import { CarouselImg } from "../../components/CarouselImg"


export class Cart extends Component {


    handleOrder() {
        localStorage.removeItem('cart')
        this.props.setCart([])
        this.props.setCartTotal({
            tax: 0,
            quantity: 0,
            total: 0,
        })
    }


    render() {

        return <div className="cart-wrapper">
            <h2>CART</h2>
            {
                this.props.cart.map((item, id) => {
                    return <div className="cart-item" key={id}>
                        <div className="product-description">
                            <h3 className="product-description-brand">{item.brand}</h3>
                            <h4 className="product-description-name">{item.name}</h4>
                            <Price prices={item.prices} />
                            <ProductAttribute setCart={this.props.setCart} attributes={item.attributes} handleAttributeSelect={this.handleAttributeChange} productId={item.id} selectedAttributes={item.selectedAttributes} />
                        </div>

                        <ChangeQuantity setCart={this.props.setCart} count={item.count} id={item.id}  selectedAttributes={item.selectedAttributes}/>
                        <CarouselImg gallery={item.gallery} />
                    </div>
                })
            }
            <Total />
            {this.props.cart.length > 0 ?
                <button
                    onClick={() => this.handleOrder()}
                    className="btn-green">
                    ORDER
                </button> : <h3>Cart is empty</h3>}

        </div>
    }
}

function mapStateToProps(state) {
    const cart = state.cart;
    return {
        cart
    };
}

function mapDispatchToProps(dispatch) {
    return ({
        setCart: (data) => { dispatch(setCart(data)) },
        setCartTotal: (data) => { dispatch(setCartTotal(data)) }
    })
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);