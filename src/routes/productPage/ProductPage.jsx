import { Component } from "react";
import { connect } from "react-redux";
import { client } from "../../GraphQL/apolloClient";
import { GET_SINGLE_PRODUCT } from "../../GraphQL/Queries";
import { setCart } from "../../redux/actions";
import './ProductPage.css'
import { ProductAttribute } from "../../components/ProductAttribute";
import Price from "../../components/Price";
import Parser from 'html-react-parser';
import deepCheck from "../../functions/deepCheck";



export class ProductPage extends Component {

    constructor() {
        super()
        this.state = {
            product: {},
            selectedImg: 0,
            selectedAttributes: {}
        };
    }

    componentDidMount() {
        client
            .query({
                query: GET_SINGLE_PRODUCT,
                variables: { productId: window.location.pathname.split('/')[2] },
                fetchPolicy: 'network-only',
            })
            .then(result => {
                this.setState({
                    product: result.data.product
                })
            });
    }

    handleImgChange = (id) => {
        this.setState({
            selectedImg: id
        })
    }


    handleAttributeSelect = (attribute, value) => {
        const newAttribute = {}
        newAttribute[attribute] = value
        this.setState({
            selectedAttributes: {
                ...this.state.selectedAttributes,
                ...newAttribute,
            }
        })
    }


    increaseQuantity(){
        return deepCheck(this.props.cart,this.state.product.id,this.state.selectedAttributes )
    }

    handleAddProductToCart() {
        const {updatedCart, changeQuantity} = this.increaseQuantity()
        if(changeQuantity){
            this.props.setCart(updatedCart)
            window.localStorage.setItem('cart', JSON.stringify(updatedCart))
        }else{
            const { description, inStock, category, ...selectedProductData } = this.state.product
            const newCartItem = { ...selectedProductData, count: 1, selectedAttributes: this.state.selectedAttributes }
            this.props.setCart([...this.props.cart, newCartItem])
            window.localStorage.setItem('cart', JSON.stringify([...this.props.cart,newCartItem ]))
        }
    }

    render() {
        return <main className="product-page-wrapper">

            {Object.keys(this.state?.product).length !== 0
                && <>

                    <div className="product-gallery">
                        {this.state?.product && this.state?.product?.gallery.map((img, id) => {
                            return <img alt=" " src={img} key={id} onClick={() => {
                                this.handleImgChange(id)
                            }} />
                        })}

                    </div>

                    <div className="product-img">
                        <img src={this.state?.product?.gallery[this.state.selectedImg]} alt=" "/>
                    </div>

                    <div className="product-description-wrapper">
                        <h3 className="product-description-brand">{this.state.product.brand}</h3>
                        <h4 className="product-description-name">{this.state.product.name}</h4>

                        <ProductAttribute 
                        attributes={this.state.product.attributes} 
                        handleAttributeSelect={this.handleAttributeSelect} 
                        selectedAttributes={this.state.selectedAttributes}
                        disabled={!this.state.product.inStock}
                        />

                        <Price prices={this.state.product.prices} />

                        <button className="btn-green add-to-cart"
                            disabled={
                                Object.keys(this.state?.product.attributes).length > Object.keys(this.state?.selectedAttributes).length || !this.state.product.inStock}
                            onClick={() => this.handleAddProductToCart()}>
                            ADD TO CART
                        </button>

                        <div className="product-description">
                            {Parser(this.state.product.description)}
                        </div>
                    </div>
                </>
            }
        </main>
    }
}


function mapStateToProps(state) {
    const cart = state.cart;
    return {
        cart
    };
}

function mapDispatchToProps(dispatch) {
    return ({
        setCart: (data) => { dispatch(setCart(data)) }
    })
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductPage);