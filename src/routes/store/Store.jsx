import { Component } from "react";
import './Store.css'
import { LOAD_PRODUCTS } from "../../GraphQL/Queries";
import { client } from "../../GraphQL/apolloClient";
import { LoadingSVG } from "../../assets/LoadingSVG";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { CartSVG } from "../../assets/CartSVG";
import { setCart } from "../../redux/actions";
import deepCheck from "../../functions/deepCheck";

export class Store extends Component {

    constructor() {
        super()
        this.state = {
            products: [],
            loading: false
        };
    }


    getStoreData(loading) {
        loading && this.setState({
            loading: true
        })
        client
            .query({
                query: LOAD_PRODUCTS,
                variables: { selectedCategory: {title:this.props.selectedCategory }},
            })
            .then(result => {
                this.setState({
                    loading: false,
                    products: result.data.category.products,
                })
            });
    }



    componentDidMount() {
        this.getStoreData(true)
    }

    componentDidUpdate(prevState){
        if(prevState.selectedCategory !== this.props.selectedCategory){
            this.getStoreData(false)
        }
    }

    handleAddToCart(product) {
        const selectedAttributes = {}
        product.attributes.forEach((attribute) => {
            selectedAttributes[attribute.name] = attribute.items[0].displayValue
        })
        const {updatedCart, changeQuantity} = deepCheck(this.props.cart, product.id, selectedAttributes)
        if(updatedCart && changeQuantity){
            this.props.setCart(updatedCart)
            window.localStorage.setItem('cart', JSON.stringify(updatedCart))
        }else{
            const { description, inStock, category, ...selectedProductData } = product
            this.props.setCart([...this.props.cart, { ...selectedProductData, count: 1, selectedAttributes }])
            window.localStorage.setItem('cart', JSON.stringify([...this.props.cart, { ...selectedProductData, count: 1, selectedAttributes }]))
        }

    }

    render() {
        return <div className="store">

            <h2 className="store-category" >{this.props.selectedCategory}</h2>
            <div className="store-items">

                {this.state.loading && <LoadingSVG />}
                {
                    this.state.products
                        .map((product, id) => {
                            return <div className={!product.inStock ? "store-item-unavailable" : ""} key={id}>
                                <Link to={product.id} >
                                    <div className="store-item" key={id} >
                                        <img className="store-item-image" src={product.gallery[0]} alt=" "/>
                                        <p className="store-item-brand">{product.brand}</p>
                                        <p className="store-item-title">{product.name}</p>
                                        <p className="store-item-price">{
                                            product.prices.map((price) => {
                                                if (price.currency.label === this.props.currency.label) {
                                                    return price.currency.symbol + " " + price.amount
                                                }
                                                return null
                                            })
                                        }</p>
                                        <div className="store-item-cart" onClick={(e) => {
                                            e.preventDefault()
                                            this.handleAddToCart(product)
                                        }}>
                                            <CartSVG />
                                        </div>
                                    </div>
                                </Link>
                            </div>
                        })
                }

            </div>
        </div>
    }
}

function mapStateToProps(state) {
    const currency = state.currency;
    const cart = state.cart;
    const selectedCategory = state.selectedCategory;
    return {
        currency,
        cart,
        selectedCategory
    };
}
function mapDispatchToProps(dispatch) {
    return ({
        setCart: (data) => { dispatch(setCart(data)) }
    })
}


export default connect(mapStateToProps, mapDispatchToProps)(Store);