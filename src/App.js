import Header from "./components/Header";
import React, { Component } from "react";
import {
  Routes,
  Route,
  Navigate
} from "react-router-dom";
import Cart from "./routes/cart/Cart";
import Store from "./routes/store/Store";
import ProductPage from "./routes/productPage/ProductPage";

export class App extends Component {

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Header />
        </header>
        <Routes>
          <Route
            path="/"
            element={<Navigate to="/store" />}
          />
          <Route
            path="/store"
            element={<Store />}
          />
          <Route
            path="/store/:id"
            element={<ProductPage />} />
          <Route
            path='/cart'
            element={<Cart />}
          />

        </Routes>
      </div>
    );
  }
}

