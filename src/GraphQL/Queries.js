import { gql } from '@apollo/client'

export const LOAD_PRODUCTS = gql`
query ($selectedCategory:CategoryInput){
	category (input:$selectedCategory){
    name
    products{
      id
      name
      brand
      inStock
      gallery
      category
      attributes{
        name
        type
        items{
          displayValue
          value
          id
        }
      }
      prices{
        amount
        currency{
          label
          symbol
        }
      }
    }
  }
}
`

export const LOAD_CURRENCIES = gql`
query{
    currencies{
      label,
      symbol
    }
}
`


export const GET_SINGLE_PRODUCT = gql`
query ($productId:String!){
  product(id:$productId){
    name
    id
    inStock
    gallery
    description
    category
    attributes{
      id
      name
      type
      items{
        displayValue
        value
        id
      }
    }
    prices{
      currency{
			label
      symbol
      }
      amount
    }
    brand
  }
}

`


export const GET_ALL_CATEGORIES = gql`
query{
  categories{
    name
  }
}
`