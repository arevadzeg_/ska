

const INIT_STATE = {
    currency: { label: "USD", symbol: "$" },
    cart: [],
    cartTotal: {
        tax: 0,
        quantity: 0,
        total: 0,
    },
    selectedCategory: 'all'
};

const appReducer = (state = INIT_STATE, action) => {
    switch (action.type) {
        case 'SET_CURRENCY': {
            return {
                ...state,
                currency: action.payload,
            }
        }
        case 'SET_CART': {
            return {
                ...state,
                cart: action.payload,
            }
        }
        case 'SET_CART_TOTAL': {
            return {
                ...state,
                cartTotal: action.payload,
            }
        }
        case 'SET_SELECTED_CATEGORY': {
            return {
                ...state,
                selectedCategory: action.payload,
            }
        }

        default:
            return state;
    }
}

export default appReducer