export const setCurrency = (payload) => {
    return {
        type: "SET_CURRENCY",
        payload: payload
    };
};
export const setCart = (payload) => {
    return {
        type: "SET_CART",
        payload: payload
    };
};

export const setCartTotal = (payload) => {
    return {
        type: "SET_CART_TOTAL",
        payload: payload
    };
};
export const setSelectedCategory = (payload) => {
    return {
        type: "SET_SELECTED_CATEGORY",
        payload: payload
    };
};
