import { Component } from "react";
import { connect } from "react-redux";
import { CartSVG } from "../assets/CartSVG";
import ChangeQuantity from "./ChangeQuantity";
import './MiniCart.css'
import Price from "./Price";
import { ProductAttribute } from "./ProductAttribute";
import { withRouter } from './../functions/withRoute'
import { setCart, setCartTotal } from "../redux/actions";


export class MiniCart extends Component {

    handleViewCart() {
        this.props.history('/cart')
        this.props.handleCartClose()
    }

    handleCheckout() {
        localStorage.removeItem('cart')
        this.props.setCart([])
        this.props.setCartTotal({
            tax: 0,
            quantity: 0,
            total: 0,
        })
    }

    render() {

        return <div>
            <span onClick={this.props.handleCartOpen} className='mini-cart-icon'>
                <CartSVG />
                <div className="cart-icon-item-count">
                    {this.props.cartTotal.quantity}
                </div>
            </span>
            {
                <div className={`mini-cart ${!this.props.cartOpen ? 'mini-cart-display-none' : ""}`}>
                    {this.props.cart.length > 0 ? <div><b>My Bag,</b> {this.props.cartTotal.quantity} items </div> :

                        <h3>
                            Cart is empty
                        </h3>
                    }
                    {
                        this.props.cart.map((item, id) => {
                            return <div className="mini-cart-item-wrapper" key={id}>
                                <div className="mini-cart-left">
                                    <div className="mini-cart-item-details">
                                        <p className="mini-cart-item-name">
                                            {item.name}
                                        </p>
                                        <div className="mini-cart-item-price">
                                            <Price prices={item.prices} />
                                        </div>
                                        <ProductAttribute
                                            setCart={this.props.setCart} 
                                            attributes={item.attributes} 
                                            productId={item.id} 
                                            selectedAttributes={item.selectedAttributes} 
                                            disabled={true}
                                            />
                                    </div>
                                    <ChangeQuantity 
                                     setCart={this.props.setCart}
                                     count={item.count} 
                                     id={item.id}
                                     selectedAttributes={item.selectedAttributes} 
                                      />
                                </div>
                                <div className="mini-cart-right">
                                    <img className="mini-cart-item-image" src={item.gallery[0]} alt=" " />
                                </div>
                            </div>
                        })
                    }

                    {
                        this.props.cart.length > 0 && <div className="mini-cart-footer">

                            <div className="mini-cart-footer-total">
                                <span>Total</span>
                                <span>{this.props.cartTotal.total}</span>
                            </div>

                            <div className="mini-cart-footer-button">
                                <button onClick={() => this.handleViewCart()} className="btn-white mini-cart-view-bag">VIEW BAG</button>
                                <button onClick={() => this.handleCheckout()} className="btn-green mini-cart-checkout">CHECK OUT</button>
                            </div>
                        </div>
                    }
                </div>

            }
            {
                this.props.cartOpen && <div onClick={this.props.handleCartClose} className="close-cart"></div>
            }
        </div>
    }
}

const MiniCartWithRoute = withRouter(MiniCart)


function mapStateToProps(state) {
    const cartTotal = state.cartTotal
    return {
        cartTotal
    };
}
function mapDispatchToProps(dispatch) {
    return ({
        setCart: (data) => { dispatch(setCart(data)) },
        setCartTotal: (data) => { dispatch(setCartTotal(data)) },
    })
}


export default connect(mapStateToProps, mapDispatchToProps)(MiniCartWithRoute);