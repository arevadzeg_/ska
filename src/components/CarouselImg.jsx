import { Component } from "react";
import { ArrowSVG } from "../assets/ArrowSVG";



export class CarouselImg extends Component {

    constructor() {
        super()
        this.state = {
            currentImg: 0
        }
    }

    handleGalleyImgChange(next) {
        let nextImg
        if (next) {
            nextImg = this.props.gallery.length - 1 > this.state.currentImg ? this.state.currentImg + 1: 0
        }
        else {
            nextImg = this.state.currentImg - 1 >= 0 ? this.state.currentImg - 1 : this.props.gallery.length - 1
        }
        this.setState({ currentImg: nextImg })
    }

    render() {


        return <div>
            <div className="product-description-img">
                <img src={this.props.gallery[this.state.currentImg]} alt='img'/>
                {this.props.gallery.length > 1 && <div className="product-img-arrows">
                    <ArrowSVG className='product-img-arrow ' onClick={() => this.handleGalleyImgChange()} />
                    <ArrowSVG className='product-img-arrow product-img-arrow-next' onClick={() => this.handleGalleyImgChange(true)} />
                </div>}
            </div>
        </div>
    }
}