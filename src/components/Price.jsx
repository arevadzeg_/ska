import { Component } from "react";
import { connect } from "react-redux";



class Price extends Component {


    render() {
        return <div className="product-price-wrapper">

            <span className="attribute-name">
                PRICE:
            </span>
            <div>
                {this.props.prices.map((price, id) => {
                    if (this.props.currency.label === price.currency.label) {
                        return <p key={id} className="product-price"> {price.currency.symbol}{price.amount}</p>
                    }
                    return null
                })}
            </div>

        </div>

    }

}


function mapStateToProps(state) {
    const currency = state.currency;
    return {
        currency,
    };
}


export default connect(mapStateToProps)(Price);