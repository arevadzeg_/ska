import _ from "lodash";
import { Component } from "react";
import { connect } from "react-redux";
import { setCartTotal } from "../redux/actions";


const TAXRATE = 0.21
export class ChangeQuantity extends Component {


    ChangeQuantity = (id, increment) => {
        const updatedCart = JSON.parse(localStorage.getItem('cart')).map((cartItem) => {
            if (cartItem.id === id) {
                if(_.isEqual(cartItem.selectedAttributes,this.props.selectedAttributes )){
                    return { ...cartItem, count: cartItem.count + (increment ? 1 : -1) }
                }
            }
            return cartItem
        }).filter((item) => item.count !== 0)
        this.updateTotal(updatedCart)
        localStorage.setItem('cart', JSON.stringify(updatedCart))
        this.props.setCart(updatedCart)
    }


    updateTotal(updatedCart) {
        let count = 0
        let totalCost = 0
        let label = ""

        updatedCart.forEach((cartItem) => {
            count += cartItem.count
            cartItem.prices.forEach((price) => {
                if (price.currency.label === this.props.currency.label) {
                    label = price.currency.symbol
                    totalCost += Math.floor(price.amount * count)
                }
            })
        })
        this.props.setCartTotal({
            quantity: count,
            total: label + " " + totalCost,
            tax: Math.floor(TAXRATE * totalCost)
        })

    }


    componentDidUpdate() {
            this.updateTotal(this.props.cart)
    }
    componentDidMount() {
        this.updateTotal(this.props.cart)
    }



    render() {

        return <div className="cart-item-quantity">
            <button className="cart-item-increment-decrement" onClick={() => this.ChangeQuantity(this.props.id, true)}>+</button>
            <span>{this.props.count}</span>
            <button className="cart-item-increment-decrement" onClick={() => this.ChangeQuantity(this.props.id, false)}>-</button>
        </div>
    }
}


function mapDispatchToProps(dispatch) {
    return ({
        setCartTotal: (data) => { dispatch(setCartTotal(data)) }
    })
}


function mapStateToProps(state) {
    const currency = state.currency
    const cart = state.cart
    return {
        currency,
        cart
    };
}

export default connect((mapStateToProps), mapDispatchToProps)(ChangeQuantity);