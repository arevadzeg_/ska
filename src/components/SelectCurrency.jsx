import { Component } from "react";
import { DropDownSVG } from "../assets/DropDownSVG";
import './SelectCurrency.css'
import { connect } from "react-redux";
import { setCurrency } from "../redux/actions";
import { LOAD_CURRENCIES } from "../GraphQL/Queries";
import { client } from "../GraphQL/apolloClient";

class SelectCurrency extends Component {

    constructor() {
        super()
        this.state = {
            dropdownOpen: false,
            options: []
        };
    }
    getCurrencies() {
        client.query({
            query: LOAD_CURRENCIES,
        }).then(result => {
            this.setState({
                options: result.data.currencies,
            })
        });
    }

    componentDidMount() {
        this.getCurrencies()
    }


    handleDropdownOpen = () => {
        if(this.props.cartOpen)this.props.handleCartClose()
        this.setState({
            dropdownOpen: true
        })
    }

    handleDropdownClose = () => {
        this.setState({
            dropdownOpen: false
        })
    }

    handleCurrencyChange = (option) => {
        this.props.handleSetCurrency(option)
        this.setState({
            dropdownOpen: false,
        })
    }

    render() {

        return <div className="currency-dropdown">
            <div className="header-dropdown" onClick={this.handleDropdownOpen}>
                {this.props.currency.symbol}
                <DropDownSVG className={`${this.state.dropdownOpen ? "currency-dropdown-icon-open" : ""}`} />
            </div>

            <div className={`currency-dropdown-options ${this.state.dropdownOpen ? "currency-dropdown-options-open" : ""}`}>
                {this.state.options.map((option, id) => {
                    return <span 
                            key={id} 
                            className={`currency-dropdown-option ${this.props.currency.label === option.label ? 'currency-dropdown-option-selected' : ""}`} 
                            onClick={() => this.handleCurrencyChange(option)}>
                        {`${option.symbol} ${option.label}`}
                    </span>
                })}
            </div>
            {
                this.state.dropdownOpen && <div className="test" onClick={this.handleDropdownClose}></div>
            }
        </div>
    }
}



function mapStateToProps(state) {
    const currency = state.currency;
    return {
        currency,
    };
}

function mapDispatchToProps(dispatch) {
    return ({
        handleSetCurrency: (data) => { dispatch(setCurrency(data)) }
    })
}

export default connect(mapStateToProps, mapDispatchToProps)(SelectCurrency);