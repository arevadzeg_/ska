import { Component } from "react";
import { connect } from "react-redux";
import { LogoSVG } from "../assets/LogoSVG";
import { setCart, setSelectedCategory } from "../redux/actions";
import './Header.css'
import SelectCurrency from "./SelectCurrency";
import MiniCart from "./MiniCart";
import { client } from "../GraphQL/apolloClient";
import { GET_ALL_CATEGORIES } from "../GraphQL/Queries";
import { withRouter } from "../functions/withRoute";

class Header extends Component {

    constructor() {
        super()
        this.state = {
            categories: [],
            cartOpen: false,
        }
    }
    handleCartOpen = () => {
        this.setState({
            cartOpen: true
        })
        document.body.classList.add("stop-scrolling");
    }

    handleCartClose = () => {
        this.setState({
            cartOpen: false
        })
        document.body.classList.remove("stop-scrolling");
    }



    componentDidMount() {
        const cart = JSON.parse(localStorage.getItem('cart'))
        cart && this.props.setCart(cart)
        client.query({
            query: GET_ALL_CATEGORIES,
        }).then((res) =>
            this.setState({
                categories: res.data.categories
            }))
    }

    handleCategoryClick(name) {
        if (window.location.pathname !== '/store') {
            this.props.history('/store')
        }
        this.handleCartClose()
        this.props.setSelectedCategory(name)
    }
    render() {
        return <header className="header">

            <nav className="header-nav">
                <ul>
                    {
                        this.state.categories.map((category, id) => {
                            return <li
                                key={id}
                                onClick={() => this.handleCategoryClick(category.name)}
                                className={`header-nav-item ${category.name === this.props.selectedCategory ? "header-nav-item-selected" : ""}`}>
                                {category.name}
                            </li>
                        })
                    }
                </ul>
            </nav>

            <LogoSVG />
            <div className="header-right">
                <SelectCurrency 
                    handleCartClose={this.handleCartClose}
                    cartOpen={this.state.cartOpen}
                    />
                <MiniCart 
                    cart={this.props.cart} 
                    setCart={this.props.setCart} 
                    handleCartOpen={this.handleCartOpen}
                    handleCartClose={this.handleCartClose}
                    cartOpen={this.state.cartOpen}
                />

            </div>

        </header>
    }
}

const headerWithRoute = withRouter(Header)

function mapDispatchToProps(dispatch) {
    return ({
        setCart: (data) => { dispatch(setCart(data)) },
        setSelectedCategory: (data) => { dispatch(setSelectedCategory(data)) }
    })
}


function mapStateToProps(state) {
    const cart = state.cart;
    const selectedCategory = state.selectedCategory;
    return {
        cart,
        selectedCategory

    };
}

export default connect((mapStateToProps), mapDispatchToProps)(headerWithRoute);