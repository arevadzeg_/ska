import { Component } from "react";



export class ProductAttribute extends Component {



    handleChange = (attributeId, value) => {
        this.props.handleAttributeSelect &&
            this.props.handleAttributeSelect(attributeId, value)
    }

    render() {
        
        return <>{
            this.props.attributes.map((attribute, id) => {
                return <div key={id} className={`attributes-wrapper ${this.props.disabled ? 'attributes-wrapper-disabled' : ""}`}>

                    <span className="attribute-name">
                        {attribute.name}:
                    </span>

                    <div >

                        {attribute.items.map((value, id) => {
                            if (attribute.name === 'Color') {
                                return <div key={id}
                                    onClick={() => this.handleChange(attribute.name, value.displayValue, this.props.productId)}
                                    style={{ background: value.value }}
                                    className={`color ${this.props.selectedAttributes[attribute.name] === value.displayValue ? 'color-selected' : ""}`}>
                                </div>
                            }
                            return <div
                                key={id}
                                onClick={() => this.handleChange(attribute.name, value.displayValue, this.props.productId)}
                                className={`attribute ${this.props.selectedAttributes[attribute.name] === value.displayValue ? 'attribute-selected' : ""}`}>
                                {value.displayValue}
                            </div>
                        })}

                    </div>
                </div>
            })
        }
        </>
    }
}