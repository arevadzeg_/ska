import { Component } from "react";
import { connect } from "react-redux";



class Total extends Component {

    render() {

        return <>{this.props.cart.length > 0 &&
            <div className="order-price">
                <div>
                    <span>Tax 21%:</span>
                    <b>
                        {this.props.cartTotal.tax}
                    </b>
                </div>
                <div>
                    <span>Quantity:</span>
                    <b>
                        {this.props.cartTotal.quantity}
                    </b>
                </div>
                <div>
                    <span className="order-price-total">Total:</span>
                    <b>
                        {this.props.cartTotal.total}
                    </b>
                </div>
            </div>
        }</>
    }
}



function mapStateToProps(state) {
    const cart = state.cart;
    const currency = state.currency;
    const cartTotal = state.cartTotal;
    return {
        cart,
        currency,
        cartTotal
    };
}

export default connect(mapStateToProps)(Total);